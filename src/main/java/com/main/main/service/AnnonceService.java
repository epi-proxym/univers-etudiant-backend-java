package com.main.main.service;

import com.main.main.entity.Annonce;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface AnnonceService {
    List<Annonce> findAll();
    void createAnnonce(Annonce annonce);
    void deleteAnnonce(Annonce annonce);
}

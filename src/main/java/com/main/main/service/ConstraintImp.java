package com.main.main.service;

import com.main.main.entity.Annonce;
import com.main.main.entity.Constraint;
import com.main.main.repo.AnnonceRepo;
import com.main.main.repo.ConstraintRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConstraintImp implements ConstraintService {
    @Autowired
    ConstraintRepo constraintRepo;

    @Override
    public List<Constraint> findAll() {
        return constraintRepo.findAll();
    }

    @Override
    public void createConstraint(Constraint constraint) {
        constraintRepo.save(constraint);
    }

    @Override
    public void deleteConstraint(Constraint constraint) {
        constraintRepo.delete(constraint);
    }


}

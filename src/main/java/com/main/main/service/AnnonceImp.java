package com.main.main.service;

import com.main.main.entity.Annonce;
import com.main.main.repo.AnnonceRepo;
import com.main.main.repo.ServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AnnonceImp implements AnnonceService {
    @Autowired
    AnnonceRepo annonceRepo;

    @Override
    public List<Annonce> findAll() {
        return annonceRepo.findAll();
    }

    @Override
    public void createAnnonce(Annonce annonce) {
        annonceRepo.save(annonce);
    }

    @Override
    public void deleteAnnonce(Annonce annonce) {
        annonceRepo.delete(annonce);
    }


}

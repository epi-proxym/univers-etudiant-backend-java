package com.main.main.service;

import com.main.main.entity.Service;
import com.main.main.repo.ServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class ServiceImp implements ServiceService {
    @Autowired
    ServiceRepo serviceRepo;

    @Override
    public List<Service> findAll() {
        return serviceRepo.findAll();
    }

    @Override
    public void createService(Service service) {
        serviceRepo.save(service);

    }

    @Override
    public void deleteService(Service service) {
        serviceRepo.delete(service);

    }
}

package com.main.main.service;

import com.main.main.entity.Organisme;
import com.main.main.repo.OrganismeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganismeImp implements ServiceOrganisme {
    @Autowired
    OrganismeRepo organismeRepo;

    @Override
    public List<Organisme> findAll() {
        return organismeRepo.findAll();
    }

    @Override
    public void createOrganisme(Organisme organisme) {
        organismeRepo.save(organisme);

    }

    @Override
    public void deleteOrganisme(Organisme organisme) {
        organismeRepo.delete(organisme);

    }
}

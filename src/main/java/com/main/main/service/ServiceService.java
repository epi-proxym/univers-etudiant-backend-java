package com.main.main.service;

import com.main.main.entity.Annonce;
import com.main.main.entity.Service;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ServiceService {
    List<Service> findAll();
    void createService(Service service);
    void deleteService(Service service);
}

package com.main.main.service;

import com.main.main.entity.Organisme;
import com.main.main.entity.Service;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ServiceOrganisme {
    List<Organisme> findAll();
    void createOrganisme(Organisme organisme);
    void deleteOrganisme(Organisme organisme);
}

package com.main.main.service;

import com.main.main.entity.Constraint;
import com.main.main.entity.Service;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ConstraintService {
    List<Constraint> findAll();
    void createConstraint(Constraint constraint);
    void deleteConstraint(Constraint constraint);
}

package com.main.main.repo;

import com.main.main.entity.Annonce;
import com.main.main.entity.Constraint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConstraintRepo extends JpaRepository<Constraint,String> {
}

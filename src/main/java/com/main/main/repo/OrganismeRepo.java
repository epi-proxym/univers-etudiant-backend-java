package com.main.main.repo;

import com.main.main.entity.Annonce;
import com.main.main.entity.Organisme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganismeRepo extends JpaRepository<Organisme,String> {
}

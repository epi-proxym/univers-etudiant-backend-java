package com.main.main.repo;

import com.main.main.entity.Annonce;
import com.main.main.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepo extends JpaRepository<Service,String> {
}

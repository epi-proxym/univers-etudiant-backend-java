package com.main.main.repo;

import com.main.main.entity.Annonce;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnnonceRepo extends JpaRepository<Annonce,String> {
}
